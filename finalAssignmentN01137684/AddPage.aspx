﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPage.aspx.cs" Inherits="finalAssignmentN01137684.AddPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        id="insert_page"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label>Page Title:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server"
                ControlToValidate="page_title"
                ErrorMessage="Enter a page title">
            </asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label>Page Content:</label></div>
         <div class="col-md-10 col-xs-12">
          <asp:TextBox ID="page_content" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server"
                ControlToValidate="page_content"
                ErrorMessage="Enter a page content">
            </asp:RequiredFieldValidator>
         </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label>Select Author:</label></div>
         <div class="col-md-10 col-xs-12">
             <asp:DropDownList id="page_author" runat="server">
                <asp:ListItem>Author1</asp:ListItem>
                <asp:ListItem>Author2</asp:ListItem>
                <asp:ListItem>Author3</asp:ListItem>
                <asp:ListItem>Author4</asp:ListItem>
                <asp:ListItem>Author5</asp:ListItem>
            </asp:DropDownList>
         </div>
    </div>
    <div class="row">
        <asp:Button Text="Add Page" Type="submit" runat="server" OnClick="AddNewPage"/>
    </div>
    <div runat="server" id="debug" class="querybox"></div>
</asp:Content>
