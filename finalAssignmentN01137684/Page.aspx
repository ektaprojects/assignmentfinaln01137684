﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="finalAssignmentN01137684.Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" CssClass="page">
    <h1 id="page_title" runat="server"></h1>
    <%--Reference from Christine's code --%>
    <asp:SqlDataSource 
        runat="server"
        id="del_class"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
    <div id="query_section" runat="server"></div>
    <asp:Button runat="server" id="del_class_btn"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <a href="EditPage.aspx?Pageid=<%Response.Write(this.pageid);%>">Edit Page</a>
    <div id="del_debug" class="querybox" runat="server"></div>

    <asp:SqlDataSource runat="server"
        id="page_content"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
    <div id="page_detail" runat="server" ></div>

    

</asp:Content>
