﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="finalAssignmentN01137684._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    
    <h2>Manage Pages</h2> 
    <asp:TextBox runat="server" id="page_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search Pages" OnClick="Search_page" />
    <br />
    <br />
    <a href="AddPage.aspx">Add New</a>
    <br />
    <asp:SqlDataSource 
    runat="server" 
    ID="pagelink_list"
    ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
<div id="page_query" runat="server" class="querybox"></div>
<br />
<asp:DataGrid ID="page_list" runat="server" CssClass="grid">

</asp:DataGrid>

    

</asp:Content>
