﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalAssignmentN01137684
{
    public partial class AddPage : System.Web.UI.Page
    {
        private string pageaddquery = "INSERT INTO PAGES" +
            "(pageid,pagetitle,pagecontent,publisheddate,author) VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void AddNewPage(object sender, EventArgs e)
        {
            string title = page_title.Text.ToString();
            string content = page_content.Text.ToString();
            string pdate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            string pageauthor = page_author.SelectedValue.ToString();

            pageaddquery += "((select max(pageid) from pages)+1,'" +
                title + "','" + content + "','"+ pdate + "','"+ pageauthor + "')";

            debug.InnerHtml = pageaddquery;
            
            insert_page.InsertCommand = pageaddquery;
            insert_page.Insert();
        }
    }
}