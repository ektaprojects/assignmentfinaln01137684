﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalAssignmentN01137684.usercontrol
{
    public partial class MenuLink : System.Web.UI.UserControl
    {
        private string sqlquery = "SELECT pagetitle,pageid FROM pages";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            page_list.SelectCommand = sqlquery;
            string link_list = Page_Link_ManualBind(page_list);

            pagemenu_link.InnerHtml = link_list;

        }
        private string Page_Link_ManualBind(SqlDataSource src)
        {
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            string links = string.Empty;

            foreach (DataRowView row in myview)
            {
                string link_menu = "<li><a href=Page.aspx?pageid=" + row["pageid"] + ">" + row["pagetitle"].ToString() + "</a></li>";
                links += link_menu;
            }
            return links;
        }
    }
}