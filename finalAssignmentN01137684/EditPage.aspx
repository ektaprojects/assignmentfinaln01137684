﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="finalAssignmentN01137684.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h3 runat="server" id="edit">Edit Page</h3>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
    <div class="row">
     <div class="col-md-2 col-xs-12"><label>Page Title:</label></div>
        <div class="col-md-10 col-xs-12">
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page title">
        </asp:RequiredFieldValidator>
       </div>
    </div>
    
    <div class="row">
     <div class="col-md-2 col-xs-12"><label>Page Content:</label></div><br />
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" ID="page_content" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="page_content_validate" controlToValidate="page_content"></asp:RequiredFieldValidator>
        </div>
    </div>
    <ASP:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>

    <div runat="server" id="debug" class="querybox"></div>
    
</asp:Content>
