﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalAssignmentN01137684
{
    public partial class _Default : Page
    {
        private string sqlquery = "SELECT pageid,pagetitle as 'Title',publisheddate AS 'Publish Date',author AS 'Author' FROM pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            pagelink_list.SelectCommand = sqlquery;
            page_query.InnerHtml = sqlquery;

            page_list.DataSource = Pages_Manual_Bind(pagelink_list);

            page_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            
            foreach (DataRow row in mytbl.Rows)
            {
                row["Title"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Title"]
                    + "</a>";
                row["Publish Date"] = row["Publish Date"];
                row["Author"] =  row["Author"];
                    
                
            }
            
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }
        //Reference from class example Student Search
        protected void Search_page(object sender, EventArgs e)
        {
            string search_sql = sqlquery + " WHERE (1=1) ";
            string search_key = page_key.Text;

            if(search_key != "")
            {
                search_sql += " AND ( pagetitle LIKE '%" + search_key + "%' ) ";
            }
            pagelink_list.SelectCommand = search_sql;
            page_query.InnerHtml = search_sql;

            page_list.DataSource = Pages_Manual_Bind(pagelink_list);
            page_list.DataBind();
        }

    }
}
