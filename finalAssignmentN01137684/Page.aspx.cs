﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalAssignmentN01137684
{
    public partial class Page : System.Web.UI.Page
    {
        private string sqlquery = "SELECT pagetitle as 'Title',pagecontent FROM pages";
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null) page_title.InnerHtml = "No pages found.";
            else
            {

                sqlquery += " WHERE PAGEID = " + pageid;

                page_content.SelectCommand = sqlquery;
                query_section.InnerHtml = sqlquery;

                DataView pagetitleview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView pagerowview = pagetitleview[0];
                string pagetitle = pagetitleview[0]["Title"].ToString();
                page_title.InnerHtml = pagetitle;

                DataView pagecontentview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView pagecontentrowview = pagecontentview[0];
                string pagecontent = pagecontentview[0]["pagecontent"].ToString();
                page_detail.InnerHtml = pagecontent;
            }

        }


        protected void DelPage(object sender, EventArgs e)
        {
            
            string delquery = "DELETE FROM PAGES WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_class.DeleteCommand = delquery;
            del_class.Delete();
            
        }
    }
}