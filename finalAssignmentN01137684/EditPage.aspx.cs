﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalAssignmentN01137684
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Reference from Christine's
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                edit.InnerHtml = "No Page Found.";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();

            page_content.Text = pagerow["pagecontent"].ToString().Substring(4);

        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            //change this
            string title = page_title.Text;
            string content = page_content.Text;
            


            string editquery = "Update pages set pagetitle='" + title + "'," +
                " pagecontent='" + content +  "'" +
                "where pageid=" + pageid;
            debug.InnerHtml = editquery;

            edit_page.UpdateCommand = editquery;
            edit_page.Update();


        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the students name outside of
            //the datagrid.
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            //If there is no student in the studentview, an invalid id is passed
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; //gets the first result which is always the student
            //string studentinfo = studentview[0][colname].ToString();
            return pagerowview;

        }
    }
}